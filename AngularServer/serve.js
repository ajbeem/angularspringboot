const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const port = 3000;
const app = express();
app.use(bodyParser.json());
app.use(cors());
app.get('/', function(req, res){
    res.send('Hola desde Node Server');
});

app.post('/guardarusuario', function(req,res){
    console.log(req.body);
    res.status(200).send({"mensaje": "Datos recibidos", "nombre": req.body.nombres})
});

app.listen(port, function(){
    console.log(`Server running at http://localhost:${port}/`)
});


/*const http = require('http');

const hostname = '127.0.0.1';
const port = 3000;

const server = http.createServer((req, res) => {
  res.statusCode = 200;
  res.setHeader('Content-Type', 'text/plain');
  res.end('Hello World');
});

server.listen(port, hostname, () => {
  console.log(`Server running at http://${hostname}:${port}/`);
});*/