import { TestBed } from '@angular/core/testing';

import { UserConectionService } from './user-conection.service';

describe('UserConectionService', () => {
  let service: UserConectionService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(UserConectionService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
