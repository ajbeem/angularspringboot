import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { UsersModel } from '../Models/users-model';

@Injectable({
  providedIn: 'root'
})
export class UserConectionService {
  _url = 'http://localhost:9898/administradorPersonas/usuarios';
  constructor(private http: HttpClient) { }

  CrearUsuario(NewUser: UsersModel){
    return this.http.post<any>(this._url, NewUser);
  }

  ObtenerUsuarios(){
    return this.http.get<UsersModel[]>(this._url+'/allusers')
  }

  ObtenerUsuarioByID(id:number){
    return this.http.get<UsersModel>(this._url+'/'+id);
  }

  UpdateUsuario(usuario: UsersModel){
    return this.http.put<any>(this._url, usuario);
  }

  DeleteUsuario(id:number){
    return this.http.delete<any>(this._url+'/'+id);
  }
}


/*
@Injectable({
  providedIn: 'root'
})
export class MensajeServiceService {
  constructor(private http: HttpClient) { }

  Url = 'http://localhost:9898/servicioUsuarios/mensajes/2';
  
  getMensaje(){
    return this.http.get<Mensajes>(this.Url);
  }

*/