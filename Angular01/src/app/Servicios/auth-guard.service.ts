import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { AutenticationService } from './autentication.service';
import { ToastrService } from 'ngx-toastr';

@Injectable({
  providedIn: 'root'
})

export class AuthGuardService implements CanActivate{

  constructor(
    private auth: AutenticationService, 
    private router: Router, 
    private toastr: ToastrService,) { }

  canActivate(router: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    if (this.auth.isUserLoggedIn())return true;
    this.router.navigate(['home']);
    this.toastr.error('Intenta Login', 'Error de Acceso', {
      timeOut: 3000,
      closeButton: true
    });
    return false;
  }
}
