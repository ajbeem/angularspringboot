import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class AutenticationService {

  constructor() { }
  
  //*Método que recibe las credenciales del usuario y hace la validación
  authenticate(username, password) {
    if (username === "root" && password === "server1root") {
      sessionStorage.setItem('username', username)
      return true;
    } else {
      return false;
    }
  }
  //*Validar si el usuario está autenticado
  isUserLoggedIn() {
    let user = sessionStorage.getItem('username')
    console.log('Estatus de la sesion:')
    console.log(!(user === null))
    return !(user === null)
  }
//* Cierre de sesión
  logOut() {
    sessionStorage.removeItem('username')
  }
}
