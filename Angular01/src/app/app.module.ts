import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { CommonModule } from '@angular/common';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ngx-toastr';

import { NuevoUsuarioCComponent } from './Usuarios/nuevo-usuario-c/nuevo-usuario-c.component';
import { GetDatosUsuarioCComponent } from './Usuarios/get-datos-usuario-c/get-datos-usuario-c.component';
import { EditDatosUsuarioCComponent } from './Usuarios/edit-datos-usuario-c/edit-datos-usuario-c.component';
import { DeleteUsuarioCComponent } from './Usuarios/delete-usuario-c/delete-usuario-c.component';
import { LoginComponent } from './Usuarios/login/login.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { HomeComponent } from './home/home.component';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { UserConectionService } from './Servicios/user-conection.service';
import { ListarUsuariosComponent } from './Usuarios/listar-usuarios/listar-usuarios.component';
import { UsersPageComponent } from './Usuarios/users-page/users-page.component';
import { LogOutComponent } from './Usuarios/log-out/log-out.component';

@NgModule({
  declarations: [
    AppComponent,
    NuevoUsuarioCComponent,
    GetDatosUsuarioCComponent,
    EditDatosUsuarioCComponent,
    DeleteUsuarioCComponent,
    LoginComponent,
    HomeComponent,
    ListarUsuariosComponent,
    UsersPageComponent,
    LogOutComponent
  ],
  imports: [
  BrowserModule,
    CommonModule,
    AppRoutingModule,
    BrowserAnimationsModule, // required animations module
    ToastrModule.forRoot(), // ToastrModule added
    NgbModule, 
    FontAwesomeModule, // Fontawesome Module added
    FormsModule,
    HttpClientModule
  ],
  providers: [UserConectionService],
  bootstrap: [AppComponent]
})
export class AppModule { }
