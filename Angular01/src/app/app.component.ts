import { Component } from '@angular/core';
import { AuthGuardService } from './Servicios/auth-guard.service';
import { AutenticationService } from './Servicios/autentication.service';
import { LoginModel } from './Models/login-model';

import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';

import { error } from 'protractor';
declare var require: any;

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent {
  title = 'Angular01';
  closeResult = '';
  LogImg = require("../assets/images/logoPC.jpg");
  loginUser = new LoginModel();
  invalidLogin = false;

  constructor(private router: Router,
    private toastr: ToastrService,
    private modalService: NgbModal,
    public auth: AutenticationService,
    private authGuard: AuthGuardService
  ) { }

  Login(content) {
    this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title' }).result.then((result) => {
      //this.closeResult = `Form Closed: ${result}`;
      this.closeResult = `Form Closed: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

  NuevoUsuario() {
    this.router.navigate(["nuevoUsuarioPath"]);
    this.toastr.success('Registro', 'Cargando Formulario', {
      timeOut: 3000,
      closeButton: true
    });
  }

  LoginUser(usr: LoginModel) {
    //this.router.navigate(["autenticarUsuario"]);
    console.log(usr);
    if (this.auth.authenticate(usr.userName, usr.pwd)) {
      this.toastr.success(`Usuario: ${usr.userName}`, 'Autenticado', {
        timeOut: 3000,
        closeButton: true
      });
      this.modalService.dismissAll('Cross click');
      this.router.navigate(['userspage']);
      this.invalidLogin = false;
    } else {
      this.toastr.error('De autorizacion', 'Error', {
        timeOut: 3000,
        closeButton: true
      });
      this.modalService.dismissAll('Cross click');
      this.invalidLogin = true;
      this.router.navigate(['home'])
    }

    /*this.auth.authenticate(usr.userName, usr.pwd)
      .subscribe(
        data => {
          this.toastr.success(`Usuario: ${data.userName}`, 'Autenticado', {
            timeOut: 3000,
            closeButton: true
          });
        }, 
        error => {
          this.toastr.error('De autorizacion', 'Error', {
            timeOut: 3000,
            closeButton: true
          });
        });*/
  }

  LogOut(){
    this.auth.logOut();
    this.toastr.error('Sesion', 'Cerrando', {
      timeOut: 2000,
      closeButton: true
    });
    this.invalidLogin = true;
    this.router.navigate(['home'])

  }

  Mensaje() {
    this.toastr.info('Mensaje', 'Mostrando', {
      timeOut: 3000,
      closeButton: true
    });
  }

  Home() {
    this.toastr.info('Home', 'Cargando', {
      timeOut: 3000,
      closeButton: true
    });
    this.router.navigate(["home"]);
  }

  EditarDatos() {
    this.toastr.info('Datos', 'Cargando', {
      timeOut: 3000,
      closeButton: true
    });
    this.router.navigate(["editUsuarioPath"]);
  }

  ListarUsuarios() {
    this.toastr.success('Usuarios', 'Cargando', {
      timeOut: 3000,
      closeButton: true
    });
    this.router.navigate(["listUsuariosPath"]);
  }
  GetUserData() {
    this.toastr.success('Usuarios', 'Cargando', {
      timeOut: 3000,
      closeButton: true
    });
    this.router.navigate(["datosUsuarioPath"]);

  }
}
