import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { NuevoUsuarioCComponent} from './Usuarios/nuevo-usuario-c/nuevo-usuario-c.component';
import { GetDatosUsuarioCComponent} from './Usuarios/get-datos-usuario-c/get-datos-usuario-c.component';
import { EditDatosUsuarioCComponent} from './Usuarios/edit-datos-usuario-c/edit-datos-usuario-c.component';
import { DeleteUsuarioCComponent} from './Usuarios/delete-usuario-c/delete-usuario-c.component';
import { LoginComponent } from './Usuarios/login/login.component';
import { HomeComponent } from './home/home.component';
import {ListarUsuariosComponent} from './Usuarios/listar-usuarios/listar-usuarios.component';
import { LogOutComponent } from './Usuarios/log-out/log-out.component';
import { UsersPageComponent } from './Usuarios/users-page/users-page.component';
import { AuthGuardService } from './Servicios/auth-guard.service';

const routes: Routes = [
  {path: "nuevoUsuarioPath", component:NuevoUsuarioCComponent },
  {path: "datosUsuarioPath", component:GetDatosUsuarioCComponent },
  {path: "editUsuarioPath", component:EditDatosUsuarioCComponent, canActivate:[AuthGuardService]},
  {path: "deleteUsuarioPath", component:DeleteUsuarioCComponent, canActivate:[AuthGuardService]},
  {path: "autenticarUsuario", component:LoginComponent},
  {path: "home", component:HomeComponent},
  {path: "listUsuariosPath", component: ListarUsuariosComponent, canActivate:[AuthGuardService]},
  {path: "loginUser", component: LoginComponent},
  {path: "logout", component: LogOutComponent},
  {path:"userspage", component:UsersPageComponent, canActivate:[AuthGuardService]}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
exports: [RouterModule]

})
export class AppRoutingModule { }
