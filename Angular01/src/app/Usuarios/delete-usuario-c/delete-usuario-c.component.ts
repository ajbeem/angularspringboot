import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UsersModel } from '../../Models/users-model';
import { ToastrService } from 'ngx-toastr';
import { UserConectionService } from '../../Servicios/user-conection.service';
import { error } from 'protractor';

@Component({
  selector: 'app-delete-usuario-c',
  templateUrl: './delete-usuario-c.component.html',
  styleUrls: ['./delete-usuario-c.component.css']
})
export class DeleteUsuarioCComponent implements OnInit {
  userModel = new UsersModel();
  constructor(
    private router:Router, 
    private toastr: ToastrService,
    private _userConection: UserConectionService,
    ) { }

  ngOnInit(): void {
    this.GetDataDeleteUser();
  }

  GetDataDeleteUser(){
    this.toastr.info("Datos del usuario -> "+localStorage.getItem("nombres"),"Obteniendo", {timeOut: 3000,closeButton: true});
    this.userModel.id= Number(localStorage.getItem("id"));
    this.userModel.nombres= localStorage.getItem("nombres");
    this.userModel.apellidoPaterno = localStorage.getItem("apellidoPaterno");
    this.userModel.email= localStorage.getItem("email");
  }

  BorrarUsuario(usuarioD: UsersModel){
    this._userConection.DeleteUsuario(usuarioD.id)
    .subscribe(
      data=>{
        this.toastr.warning("Con ID:"+data.ID_usuario+"->"+data.mensajeD,"Usuario",{timeOut:3000,closeButton:true}),
        this.router.navigate(["home"])
      },
      error=>{
        this.toastr.error(error,"No se logro el borrado",{timeOut:3000,closeButton:true}),
        this.router.navigate(["deleteUsuarioPath"])
      }
    );
  }

  CancelarBorrado(){
    this.router.navigate(["home"]);
  }

}
