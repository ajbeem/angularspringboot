import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DeleteUsuarioCComponent } from './delete-usuario-c.component';

describe('DeleteUsuarioCComponent', () => {
  let component: DeleteUsuarioCComponent;
  let fixture: ComponentFixture<DeleteUsuarioCComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DeleteUsuarioCComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeleteUsuarioCComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
