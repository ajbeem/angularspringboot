import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GetDatosUsuarioCComponent } from './get-datos-usuario-c.component';

describe('GetDatosUsuarioCComponent', () => {
  let component: GetDatosUsuarioCComponent;
  let fixture: ComponentFixture<GetDatosUsuarioCComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GetDatosUsuarioCComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GetDatosUsuarioCComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
