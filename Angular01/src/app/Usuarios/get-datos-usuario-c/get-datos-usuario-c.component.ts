import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { UsersModel } from '../../Models/users-model';
import { UserConectionService } from '../../Servicios/user-conection.service';
import { Usuarios } from '../../../../../Angular00/src/app/Models/Usuarios';
import { error } from '@angular/compiler/src/util';

@Component({
  selector: 'app-get-datos-usuario-c',
  templateUrl: './get-datos-usuario-c.component.html',
  styleUrls: ['./get-datos-usuario-c.component.css']
})
export class GetDatosUsuarioCComponent implements OnInit {
  usuario = new UsersModel();
  constructor(
    private router:Router, 
    private toastr: ToastrService,
    private service: UserConectionService,
    ) { }

  ngOnInit(): void {
    this.usuario.id=0;
  }

  getDataUser(usr:UsersModel){
    this.service.ObtenerUsuarioByID(usr.id)
    .subscribe(
      data=>{
        this.usuario= data;
        this.toastr.success("Datos de Usuario"+data.nombres, 'Recuperando', {timeOut: 3000,closeButton: true});
      },
      error=>{
        this.toastr.error("Al obtener los datos"+error, 'Error', {timeOut: 3000,closeButton: true});
      }
    );

  }
  
  _EditUser(usuarioEdit: UsersModel):void{
    localStorage.setItem("id",usuarioEdit.id.toString());
    localStorage.setItem("nombres", usuarioEdit.nombres.toString());
    localStorage.setItem("apellidoPaterno",usuarioEdit.apellidoPaterno.toString());
    localStorage.setItem("apellidoMaterno",usuarioEdit.apellidoMaterno.toString());
    localStorage.setItem("edad",usuarioEdit.edad.toString());
    localStorage.setItem("password",usuarioEdit.password.toString());
    this.toastr.warning("Usuario", 'Editando: ', {timeOut: 3000,closeButton: true});
    this.router.navigate(["editUsuarioPath"]);
  }

  _DeleteUser(deleteUsuario: UsersModel) {
    this.toastr.error("Usuario:"+deleteUsuario.id, 'Borrando ', {timeOut: 3000,closeButton: true});
  }

}
