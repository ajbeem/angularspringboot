import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NuevoUsuarioCComponent } from './nuevo-usuario-c.component';

describe('NuevoUsuarioCComponent', () => {
  let component: NuevoUsuarioCComponent;
  let fixture: ComponentFixture<NuevoUsuarioCComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NuevoUsuarioCComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NuevoUsuarioCComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
