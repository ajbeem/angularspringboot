import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { UsersModel } from '../../Models/users-model';
import { UserConectionService } from '../../Servicios/user-conection.service';

@Component({
  selector: 'app-nuevo-usuario-c',
  templateUrl: './nuevo-usuario-c.component.html',
  styleUrls: ['./nuevo-usuario-c.component.css']
})
export class NuevoUsuarioCComponent implements OnInit {
  userModel = new UsersModel();
  constructor(
    private router:Router, 
    private toastr: ToastrService,
    private _userConection: UserConectionService,
    ) { }

  ngOnInit(): void {
  }

  enviarDatos(){
    console.log(this.userModel);
    this._userConection.CrearUsuario(this.userModel)    
    .subscribe(
      data=>     this.toastr.info(data.mensaje, 'Usuario registrado: ', {timeOut: 3000,closeButton: true}),
      error=>this.toastr.error('Al enviar los Datos', 'Error', {timeOut: 3000,
        closeButton: true})
    );

    this.router.navigate(["home"]);
    /*    

    .subscribe(
      data =>   console.log('Exito', data),
      error=>   console.error('Falla!', error)
    );
    */
  }

}
