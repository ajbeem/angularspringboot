import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { UsersModel } from '../../Models/users-model';
import { UserConectionService } from '../../Servicios/user-conection.service';
import { Usuarios } from '../../../../../Angular00/src/app/Models/Usuarios';
import { error } from '@angular/compiler/src/util';

@Component({
  selector: 'app-listar-usuarios',
  templateUrl: './listar-usuarios.component.html',
  styleUrls: ['./listar-usuarios.component.css']
})
export class ListarUsuariosComponent implements OnInit {
  userModel = new UsersModel();
  usuarios: UsersModel[]; 
  constructor(
    private router:Router, 
    private toastr: ToastrService,
    private service: UserConectionService,
    ) { }

  ngOnInit(): void {
    this.UsersList();
  }

  UsersList(){
    this.service.ObtenerUsuarios()
    .subscribe(
      data=>{
      this.usuarios=data;
      this.toastr.success("Datos de Usuarios", 'Recuperando', {timeOut: 3000,closeButton: true});
      }, 
      error=>{
        this.toastr.error("Al obtener los datos", 'Error', {timeOut: 3000,closeButton: true});
      }
    )
  }

  Editar(usuarioEdit: UsersModel):void{
    localStorage.setItem("id",usuarioEdit.id.toString());
    localStorage.setItem("nombres", usuarioEdit.nombres.toString());
    localStorage.setItem("apellidoPaterno",usuarioEdit.apellidoPaterno.toString());
    localStorage.setItem("apellidoMaterno",usuarioEdit.apellidoMaterno.toString());
    localStorage.setItem("email", usuarioEdit.email.toString());
    localStorage.setItem("edad",usuarioEdit.edad.toString());
    localStorage.setItem("password",usuarioEdit.password.toString());

    /*public id: number;
    public nombres: String;
    public apellidoPaterno: String;
    public apellidoMaterno: String;
    public email: String;
    public edad: Number;
    public password: String;
    public newsLetter: Boolean;*/
    this.toastr.warning("Usuario", 'Editando: ', {timeOut: 3000,closeButton: true});
    this.router.navigate(["editUsuarioPath"]);
  }

  Eliminar(usuarioDelete:UsersModel){
    localStorage.setItem("id",usuarioDelete.id.toString());
    localStorage.setItem("nombres", usuarioDelete.nombres.toString());
    localStorage.setItem("apellidoPaterno",usuarioDelete.apellidoPaterno.toString());
    localStorage.setItem("email", usuarioDelete.email.toString());
    this.router.navigate(["deleteUsuarioPath"]);
  }

}
