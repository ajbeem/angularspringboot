import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditDatosUsuarioCComponent } from './edit-datos-usuario-c.component';

describe('EditDatosUsuarioCComponent', () => {
  let component: EditDatosUsuarioCComponent;
  let fixture: ComponentFixture<EditDatosUsuarioCComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditDatosUsuarioCComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditDatosUsuarioCComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
