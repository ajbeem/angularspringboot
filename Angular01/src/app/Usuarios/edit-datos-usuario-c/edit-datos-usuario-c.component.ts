import { Component, OnInit } from '@angular/core';
import { UsersModel } from '../../Models/users-model';
import { UserConectionService } from '../../Servicios/user-conection.service';
import { error } from '@angular/compiler/src/util';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-edit-datos-usuario-c',  
  templateUrl: './edit-datos-usuario-c.component.html', 
  styleUrls: ['./edit-datos-usuario-c.component.css']
})
export class EditDatosUsuarioCComponent implements OnInit {
  topicsHasError = true;
  validarOpcion(value){
    if(value=== 'default'){
      this.topicsHasError = true;
    }else{
      this.topicsHasError = false;
    }
  }

  jsonPerson = {
    "nombre": "Alfredo",
    "apellido": "jimenez"
  };

  topics=['angular', 'React', 'Vue'];
  //userModel = new UsersModel('Alfredo','Jimenez','Miguel','fred@gmail.com',45,'123456',true);
  userModel= new UsersModel();
  constructor(private _service: UserConectionService,
    private router:Router, 
    private toastr: ToastrService) { }
  ngOnInit(): void { 
    this.getData();
   }

  getData(){
    this.toastr.info("Datos del usuario -> "+localStorage.getItem("nombres"),"Obteniendo", {timeOut: 3000,closeButton: true});
    this.userModel.id= Number(localStorage.getItem("id"));
    this.userModel.nombres= localStorage.getItem("nombres");
    this.userModel.apellidoPaterno = localStorage.getItem("apellidoPaterno");
    this.userModel.apellidoMaterno = localStorage.getItem("apellidoMaterno");
    this.userModel.edad = Number(localStorage.getItem("edad"));
    this.userModel.email= localStorage.getItem("email");
    this.userModel.password = localStorage.getItem("password");
    /*this._service.ObtenerUsuarioByID(+id)
    .subscribe(
      data=>{
        this.userModel=data;
      },
      error=>{
        this.toastr.error("Al obtener los datos: ","Errror: ", {timeOut: 3000,closeButton: true});
      }
    );*/
  }

  actualizarUsuario(usuario: UsersModel){
    this._service.UpdateUsuario(usuario)
    .subscribe(
      data=>{
        this.toastr.info(data.Usuario +" -> "+data.mensaje, 'Usuario: ', {timeOut: 3000,closeButton: true}),
        this.router.navigate(["home"])
      }, 
      error=>{
        this.toastr.error(error, 'Error al enviar los Datos', {timeOut: 3000,closeButton: true}),
        this.router.navigate(["editUsuarioPath"])
      }
    );
  }

  cancelar(){
    this.router.navigate(["home"]);
  }

}
