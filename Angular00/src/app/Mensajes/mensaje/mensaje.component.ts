import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Mensajes } from '../../Models/Mensajes';
import { MensajeServiceService } from '../../Service/mensaje-service.service';

@Component({
  selector: 'app-mensaje',
  templateUrl: './mensaje.component.html',
  styleUrls: ['./mensaje.component.css']
})
export class MensajeComponent implements OnInit {
  mensaje:Mensajes;
  constructor(private service:MensajeServiceService, private router:Router) { }

  ngOnInit(): void {
    this.service.getMensaje()
    .subscribe(data=>{
      this.mensaje=data;
    })

  }

}
