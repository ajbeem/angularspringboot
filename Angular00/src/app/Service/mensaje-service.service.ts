import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Mensajes } from '../Models/Mensajes';

@Injectable({
  providedIn: 'root'
})
export class MensajeServiceService {
  constructor(private http: HttpClient) { }

  Url = 'http://localhost:9898/servicioUsuarios/mensajes/2';
  
  getMensaje(){
    return this.http.get<Mensajes>(this.Url);
  }
}
