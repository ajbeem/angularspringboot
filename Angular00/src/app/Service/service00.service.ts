import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Usuarios } from '../Models/Usuarios';
import { Mensajes } from '../Models/Mensajes';

@Injectable({
  providedIn: 'root'
})
export class Service00Service {
  //user: Usuarios
  constructor(private http: HttpClient) { }
  Url = 'http://localhost:9898/servicioUsuarios/usuarios';

  createUser(usuario:Usuarios){
    return this.http.post<Usuarios>(this.Url,usuario);
  }

  getUsuarios(){
    return this.http.get<Usuarios[]>(this.Url);
  }

  getUser(id:number){
    return this.http.get<Usuarios>(this.Url+"/"+id);
  }

  updateUser(usuario:Usuarios){
    return this.http.put<Usuarios>(this.Url+"/"+usuario.idPersona, usuario);
  }

  deleteUsr(id:number){
    return this.http.delete(this.Url+"/"+id);
  }
}
