import { TestBed } from '@angular/core/testing';

import { Service00Service } from './service00.service';

describe('Service00Service', () => {
  let service: Service00Service;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(Service00Service);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
