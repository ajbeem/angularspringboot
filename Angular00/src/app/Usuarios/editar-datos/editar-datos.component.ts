import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Service00Service } from '../../Service/service00.service';
import { Usuarios } from '../../Models/Usuarios';

@Component({
  selector: 'app-editar-datos',
  templateUrl: './editar-datos.component.html',
  styleUrls: ['./editar-datos.component.css']
})
export class EditarDatosComponent implements OnInit {

  constructor(private router:Router, private service:Service00Service) { }

  ngOnInit(): void {
    this.ActualizarDatos();
  }
  usr : Usuarios
  ActualizarDatos(){
    let id= localStorage.getItem("idPersona");
    this.service.getUser(+id)
    .subscribe(data=>{
      this.usr=data;
    })
  }
}
