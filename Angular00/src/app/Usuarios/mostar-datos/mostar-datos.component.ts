import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Usuarios } from '../../Models/Usuarios';
import { Service00Service } from '../../Service/service00.service';

@Component({
  selector: 'app-mostar-datos',
  templateUrl: './mostar-datos.component.html',
  styleUrls: ['./mostar-datos.component.css']
})
export class MostarDatosComponent implements OnInit {
  usuarios:Usuarios[];

  constructor(private service:Service00Service, private router:Router) { }

  ngOnInit(): void {
    this.service.getUsuarios()
    .subscribe(data=>{
      this.usuarios=data;
    })
  }

  Editar(usuario:Usuarios):void{
    localStorage.setItem("id",usuario.idPersona.toString());
    this.router.navigate(["editarDatos"]);
  }

  Eliminar(){
    this.router.navigate(["eliminar"]);
  }

}
