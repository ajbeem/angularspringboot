import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MostarDatosComponent } from './mostar-datos.component';

describe('MostarDatosComponent', () => {
  let component: MostarDatosComponent;
  let fixture: ComponentFixture<MostarDatosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MostarDatosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MostarDatosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
