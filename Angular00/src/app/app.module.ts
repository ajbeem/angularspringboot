import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MostarDatosComponent } from './Usuarios/mostar-datos/mostar-datos.component';
import { NuevoUsuarioComponent } from './Usuarios/nuevo-usuario/nuevo-usuario.component';
import { EditarDatosComponent } from './Usuarios/editar-datos/editar-datos.component';
import { EliminarUsuarioComponent } from './Usuarios/eliminar-usuario/eliminar-usuario.component';
import { FormsModule } from '@angular/forms';
import { Service00Service } from './Service/service00.service';
import { HttpClientModule } from '@angular/common/http';
import { MensajeComponent } from './Mensajes/mensaje/mensaje.component'

@NgModule({
  declarations: [
    AppComponent,
    MostarDatosComponent,
    NuevoUsuarioComponent,
    EditarDatosComponent,
    EliminarUsuarioComponent,
    MensajeComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [Service00Service],
  bootstrap: [AppComponent]
})
export class AppModule { }
