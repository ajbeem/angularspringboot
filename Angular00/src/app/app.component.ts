import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Angular00';
  constructor (private router:Router){}
  NuevoUsuario(){
    this.router.navigate(["nuevoUsuario"]);
  }
  MostrarDatos(){
    this.router.navigate(["mostrarDatos"]);
  }
  /*EditarDatos(){
    this.router.navigate(["editarDatos"])
  }
  EliminarUsuario(){
    this.router.navigate(["eliminar"])
  }*/

  ObtenerMensajes(){
    this.router.navigate(["getmensajes"])
  }
}
