import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MostarDatosComponent } from './Usuarios/mostar-datos/mostar-datos.component';
import { NuevoUsuarioComponent } from './Usuarios/nuevo-usuario/nuevo-usuario.component';
import { EditarDatosComponent } from './Usuarios/editar-datos/editar-datos.component';
import { EliminarUsuarioComponent } from './Usuarios/eliminar-usuario/eliminar-usuario.component';
import { MensajeComponent } from './Mensajes/mensaje/mensaje.component';


const routes: Routes = [
  {path: 'nuevoUsuario', component:NuevoUsuarioComponent},
  {path: 'mostrarDatos', component:MostarDatosComponent},
  {path: 'editarDatos', component:EditarDatosComponent},
  {path: 'eliminar', component: EliminarUsuarioComponent},
  {path: 'getmensajes', component: MensajeComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],

exports: [RouterModule]
})
export class AppRoutingModule { }
